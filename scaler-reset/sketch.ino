#define Relay1 2
#define Relay2 3

void setup()
{
  Serial.begin(9600);
  pinMode(Relay1, OUTPUT);
  pinMode(Relay2, OUTPUT);
  digitalWrite(Relay1, HIGH);
  digitalWrite(Relay2, HIGH);
  delay(5000);
  digitalWrite(Relay1, LOW);
  digitalWrite(Relay2, LOW);
}

void loop() {}


